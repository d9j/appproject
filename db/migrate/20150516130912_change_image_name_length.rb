class ChangeImageNameLength < ActiveRecord::Migration
  def change
    change_table :images do
      change_column :images,:image_file_name,:string,{:length => 20,:default => ''}
    end
  end
end
