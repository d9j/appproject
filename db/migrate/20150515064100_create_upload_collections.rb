class CreateUploadCollections < ActiveRecord::Migration
  def change
    create_table :upload_collections do |t|
      t.string :upload_token,null: false,length: 40
      t.timestamps null: false
    end
    add_index(:upload_collections,:upload_token,unique: false)
  end
end
