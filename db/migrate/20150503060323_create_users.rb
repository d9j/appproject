class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :session_id,limit: 256, null: false
      t.timestamps null: false
    end
    add_index :users, [:session_id]
  end
end
