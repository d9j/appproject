class AddUniqueIndex < ActiveRecord::Migration
  def change
    change_column(:images,:image_file_name,:string,{:limit => 20})
    change_column(:images,:file_hash,:string,{:limit => 20})
    add_index(:images,:file_hash,{:unique => true})
    add_index(:images,:image_file_name,{:unique => true})
  end
end
