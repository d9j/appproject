# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150519061801) do

  create_table "images", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "image_file_name",    limit: 20,  default: ""
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "file_hash",          limit: 20
  end

  add_index "images", ["file_hash"], name: "index_images_on_file_hash", unique: true, using: :btree
  add_index "images", ["image_file_name"], name: "index_images_on_image_file_name", unique: true, using: :btree

  create_table "upload_collections", force: :cascade do |t|
    t.string   "upload_token", limit: 255, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "upload_collections", ["upload_token"], name: "index_upload_collections_on_upload_token", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "session_id", limit: 256, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "users", ["session_id"], name: "index_users_on_session_id", length: {"session_id"=>255}, using: :btree

end
