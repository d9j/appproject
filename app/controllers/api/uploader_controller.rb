class Api::UploaderController < Api::BaseApiController
  before_action :only => [:image_url,:img_upload] do
    user = User.find_or_create(session.id)
    unless user
      render json: {:response =>"Unknown error"}
    end
    @uploader = ImageUploader.new(user)
  end
  def image_url
    params.require(:img_url)
    img = @uploader.upload(params)
    upload_result(img)
  end
  def img_upload
    params.require(:file)
    img =  @uploader.upload(params)
    upload_result(img)
  end
  private
 def upload_result(img)
   if img.valid?
    #return  render json: {:response => t('upload_results.failed_upload')}, status: 400
     return render json: {:response => t('upload_results.upload_successful'), :file_hash => img.file_hash}
   end
   render json: {:response => t('upload_results.failed_upload')}, status: 400
 end
end
