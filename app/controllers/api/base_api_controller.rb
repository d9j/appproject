class Api::BaseApiController < ApplicationController
  protect_from_forgery only: []
  respond_to :json
end