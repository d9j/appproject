class Api::ImageController <  Api::BaseApiController

  before_action do
    @user = User.find_or_create(session.id)
  end
  def list
    params.permit(:images)
    res = Image.list_images(params[:images])
    unless res
      return  render json: {:response => t('not_found_img')}, status: 400
    end
    render json: {:response => t('found_img'), :img_list => res}
  end
end