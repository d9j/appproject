// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery/dist/jquery.min.js
// require jquery_ujs
// require turbolinks
//= require jquery
//= require jquery_ujs
//= require bootstrap/dist/js/bootstrap.min
//require turbolinks
//= require angular
//= require angular-animate
//= require angular-ui-router
//= require angular-rails-templates
//= require angular-translate
//= require ui-bootstrap-custom-build/ui-bootstrap-custom-0.12.1.min
//= require ui-bootstrap-custom-build/ui-bootstrap-custom-tpls-0.12.1.min
//= require ng-file-upload
//= require ng-file-upload-shim
//= require lodash
//= require restangular
//= require_tree .

