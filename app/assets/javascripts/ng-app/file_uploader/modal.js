angular.module('fileUploader.modal',['ui.bootstrap.tpls','ui.bootstrap.modal'])
    .controller('ModalCtrl',['$scope','$modal','$log',function($scope,$modal,$log){
        $scope.open = function () {
            var modalInstance = $modal.open({
                templateUrl: 'uploadFileContent.html',
                controller: 'ModalInstanceCtrl'
            });
            modalInstance.result.then(function () {
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }])
.controller('ModalInstanceCtrl',['$scope','$modalStack','$modalInstance','$rootScope',function($scope,$modalStack,$modalInstance,$rootScope){
        $rootScope.$on('uploadDone',function(event, arguments){
            $modalInstance.dismiss('close');
        });
    $scope.ok = function () {
        $modalInstance.close('close');
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);


