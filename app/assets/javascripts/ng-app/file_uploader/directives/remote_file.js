angular.module('fileUploader.directives.remoteFile',[])
    .directive('remoteFile',['$http','$timeout',function($http,$timeout){
    return {
        require: 'ngModel',
        restrict: 'A',
        controller: 'FileCtrl',
        link: function(scope, element, attrs, FileCtrl){
            element.on('paste',function(){
                $timeout(function(){
                    scope.pastedImage(prepareUrl(element.val()));
                    element.val("");
                    scope.$apply();
                 //   scope.$apply();
                },5);
            });

            function prepareUrl(file_url)
            {
                var file_name = file_url;
                //this removes the anchor at the end, if there is one
                file_name = file_name.substring(0, (file_url.indexOf("#") == -1) ? file_url.length : url.indexOf("#"));
                //this removes the query after the file name, if there is one
                file_name = file_name.substring(0, (file_url.indexOf("?") == -1) ? file_url.length : file_url.indexOf("?"));
                //this removes everything before the last slash in the path
                file_name = file_name.substring(file_url.lastIndexOf("/") + 1, file_url.length);
                var regexp =new RegExp('^(?:f|ht)tps?:\/\/','i');
                if(!regexp.exec(file_url)){
                    file_url = "https://" + file_url;
                }

                if(file_name.length ==0){
                    file_name = file_url
                }
                return {
                    name: file_name,
                    url:  file_url,
                    isRemote: true
                }
            }

        }
    }
}])
    .directive('focusInput',['$timeout',function($timeout){
        return {
            restrict: 'A',
            link:  function(scope, element, attrs, ctrl){
                var focus_elem = attrs.focusInput;
                element.on('click',function(){
                    $timeout(function(){
                        $(focus_elem).focus();
                      //  scope.$apply();
                    });
                });
            }
        }
    }]);