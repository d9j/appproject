
angular.module('fileUploader.uploadService',['ngFileUpload','fileUploader.uploadHelper'])

.service('uploadService',['Upload','uploadHelper','$q','$timeout',
        function(Upload,uploadHelper,$q,$timeout){

        this.addedFiles = [];
        this.uploadIndex = 0;

        this.prepareFiles = function(file_set){
            if(angular.isArray(file_set)){
                for(var i = 0; i < file_set.length; i++){
                    this._addFile(file_set[i]);
                }
            }else if(file_set.isRemote){
                this._addFile(file_set);
            }
            return this.addedFiles;
        };
        this._uploadFactory = function(image){
            var _url = image.isRemote ? '/api/upload/image_url' : '/api/upload/img_upload';
            if(image.isRemote){
                return Upload.upload({url: _url,
                    method: 'POST', fields: {
                        'img_url': image.dataUrl
                    },
                    file: image
                });
            }else {
                return  Upload.upload({url: _url, method: 'POST', file: image});
            }
        };
        this.uploadQue = function(){
            var fileUpload = this.addedFiles;
            var deferred = $q.defer();
            var promise = deferred.promise;
            var filesHash = [];
            var next = 0;
            var that = this;
            var uploadExec = function(){
                var image = fileUpload[next];
                that._uploadFactory(image)
                    .progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        var data = { uploadId: image.uploadId,  progress: progressPercentage };
                        deferred.notify ? deferred.notify(data) :
                        promise.progress_fn && $timeout(function () {
                            promise.progress_fn(data)
                        });
                    })
                    .success(function (data, status, headers, config) {
                        config.file.uploaded = true;
                        filesHash.push(data.file_hash);
                        if(next++ != fileUpload.length-1){
                            uploadExec();
                        }else{
                            promise.uploadDone_fn && $timeout(function () {
                                promise.uploadDone_fn(filesHash)
                            });
                        }
                    })
                    .error(function (data, status, headers, config){
                       promise.error_fn && $timeout(function () {
                           promise.error_fn(image.uploadId)
                       });
                        if(next++ != fileUpload.length-1){
                            uploadExec();
                        }else{
                            promise.uploadDone_fn && $timeout(function () {
                                promise.uploadDone_fn(filesHash)
                            });
                        }
                    });
            };

            if(this.addedFiles.length > 0){
                $timeout(function(){
                    promise.upload_started_fn && $timeout(function () {
                        console.log("Upload Started");
                        promise.upload_started_fn()
                    });
                    uploadExec();
                });
            }
            promise.uploadStarted = function(fn){
                promise.upload_started_fn = fn;
                return promise;
            };

            promise.progress = function (fn) {
                promise.progress_fn = fn;
                promise.then(null, null, function (update) {
                    fn(update);
                });
                return promise;
            };

            promise.error = function(fn){
                promise.error_fn = fn;
                return promise;
            };

            promise.uploadDone = function(fn){
                promise.uploadDone_fn = fn;
                return promise;
            };

            return promise;
        };

        this.removeFromList = function(upid){
            for(var i =0 ; i < this.addedFiles.length; i++ ){
                if(this.addedFiles[i].uploadId == upid){
                    this.addedFiles.splice(i,1);
                    break;
                }
            }
            return this.addedFiles;
        };

        this._addFile = function(file){
            // We don't need duplicates
            if(this._isAdded(file.name)){
                return;
            }
            var mFile =  uploadHelper.fileFactory(file,this.uploadIndex);
            this.uploadIndex++;
            this.addedFiles.push(mFile);
        };
        this._isAdded = function(file_name){
            var bool_added = false;
            for(var i =0; i < this.addedFiles.length; i++){
                if(this.addedFiles[i].name == file_name){
                    bool_added = true;
                    break;
                }
            }
            return bool_added;
        };

}]);

