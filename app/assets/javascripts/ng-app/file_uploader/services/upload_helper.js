
angular.module('fileUploader.uploadHelper',
    ['ngFileUpload'])
    .service('uploadHelper',['$timeout',function($timeout){
    this.generateThumb = function(file){
        if (file != null) {
            if (file.type.indexOf('image') > -1) {
                $timeout(function() {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function(e) {
                        $timeout(function() {
                            file.dataUrl = e.target.result;
                        });
                    }
                });
            }
        }
    };
   this.fileFactory = function(file, uploadIndex)    {
       if( file.isRemote){
           file.dataUrl = file.url
       }else{
           file.dataUrl = this.generateThumb(file);
       }
       file.uploadId = uploadIndex;
       return file;
   }
}]);
