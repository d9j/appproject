var upl = angular.module('fileUploader.uploader',[
    'fileUploader.directives.uploadProcess',
    'fileUploader.directives.remoteFile',
    'fileUploader.uploadService']);
upl.controller('FileCtrl', [
    '$scope',
    '$state','uploadService','$window', function ($scope,$state,
                                                  uploadService,$window) {
    $scope.addedFiles = [];
    $scope.$watch('files', function () {
        if($scope.files && $scope.files.length){
            $scope.addedFiles = uploadService.prepareFiles($scope.files);
        }
    });
    $scope.$on('uploadDone',function(event, files_hash){
        if(files_hash.length){
            $window.location.href =  $state.href('image_shower',{images: files_hash.join(",") });
        }else{
            // Just reload page
            $window.location.reload();
        }
    });

        $scope.pastedImage = function(url){
            $scope.addedFiles = uploadService.prepareFiles(url);
        };
    $scope.removeFromList = function(upid){
        $scope.addedFiles = uploadService.removeFromList(upid);
    };
    $scope.uploadFiles = function(){
        uploadService.uploadQue()
            .uploadStarted(function(){
                $scope.$broadcast('uploadStarted');
            })
            .uploadDone(function(filesHash){
                $scope.$emit('uploadDone', filesHash);
            })
            .progress(function(data){
                $(".upload_"+data.uploadId).text('progress: ' + data.progress + '%')
            })
             .error(function(uploadId){
                $(".upload_"+uploadId).text('Failed to upload');
             })
    };
}]);