
var shower = angular.module('imageShower',['imageShower.image_resolver'])
    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            .state('image_shower',{
                url: '/api/images/list/:images',
                templateUrl: '/templates/upload_list.tpl.html',
                resolve:{
                  images: ['ImageResolver','$stateParams',function(ImageResolver,$stateParams){
                      return ImageResolver.getImages($stateParams.images).$promise;
                  }]
                },
                controller: 'ShowImagesCtrl'
            })

    });
shower.controller('ShowImagesCtrl',['images',function(images){
    console.log(images);

}]);

