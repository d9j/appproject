angular.module('imageShower.image_resolver',[])

.factory('ImageResolver',['$http','$state',function($http,$state){

        return {
            getImages: function(image_hash){
                return $http.get($state.href('image_shower',{images: image_hash}));
            }
        }

    }]);