var application = angular.module('AppProject',
                            ['ngAnimate','i18n',
                                'ui.router',
                                'fileUploader',
                                'imageShower',
                            'restangular'])
    .config(function($stateProvider, $urlRouterProvider,
        $locationProvider, $httpProvider,RestangularProvider){
        $locationProvider.html5Mode(true).hashPrefix('!')
        RestangularProvider.setBaseUrl('/api');
        RestangularProvider.setRequestSuffix('.json');
        RestangularProvider.setRequestInterceptor(function(elem, operation) {
            if (operation === "remove") {
                return null;
            }
            return elem;
        });
        $httpProvider.defaults.headers.common['X-CSRF-Token'] =
            $('meta[name=csrf-token]').attr('content');
        $stateProvider
            .state('home',{
                url: '/',
                templateUrl: '/templates/home.html',
                controller: 'HomeCtrl'
            });
    //    $urlRouterProvider.otherwise('/');

        // enable HTML5 Mode for SEO

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
});

application.controller('HomeCtrl',['$scope',function($scope,$translateProvider){


}]);
application.controller('HeaderCtrl',['$scope',function($scope){
    $scope.modalClick = function(){

    }

}]);