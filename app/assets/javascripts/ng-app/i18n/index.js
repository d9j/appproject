angular.module('i18n',['i18n_en','i18n_zh']).config(function($translateProvider){
    $translateProvider.preferredLanguage('en');
});