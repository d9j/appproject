angular.module('i18n_en',['pascalprecht.translate'])
        .config(function($translateProvider){
        $translateProvider.translations('en',{
            HEADLINE: 'My Headline',
            HWORLD: 'Hello world'
        });
    });