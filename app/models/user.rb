class User < ActiveRecord::Base
  has_many :images

  def self.find_or_create(session_id)
    user = self.where(:session_id => session_id).first
    unless user
      user = User.new(:session_id => session_id)
      user.save
    end
    user
  end


end
