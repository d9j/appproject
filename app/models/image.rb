class Image < ActiveRecord::Base
  belongs_to :user
  attr_accessor :image_remote_url
  has_attached_file :image, :styles => { :medium => ["250x250>",:jpg],
                                         :thumb => ["180x180>",:jpg] },
                    :default_url => "/images/usr/:style/missing.png",
                    :path => ':rails_root/public/images/usr/:user_id/:id/:style/:filename',
                    :url  => '/images/usr/:user_id/:id/:style/:filename'
  validates_attachment :image,
                       :content_type =>{ :content_type =>["image/jpeg","image/png","image/gif"]},
                       :size => { :in => 0..5.megabytes }
  validates_presence_of :image
  before_save :normalized_name
  def picture_from_url(url)
    if UrlUtil.is_url(url)
      self.image = URI.parse(url)
      # Assuming url_value is http://example.com/photos/face.png
      # image_file_name == "face.png"
      # image_content_type == "image/png"
      @image_remote_url = url
    end
  end
  def self.list_images(params, format = :original)
    hash_list = params.split(",")
    if hash_list.empty?
      return nil
    end
    urls = []
    hash_list.each do |img_hash|
     res =  where(file_hash: img_hash ).first
      if res.present?
        urls.push(res.image.url(format))
      end
    end
    urls
  end
  def normalized_name
    hash = FileUtil.generate_hash
    extension = File.extname(self.image_file_name).downcase
    self.file_hash = hash
    self.image_file_name =  hash + extension
  end
  # interpolate in paperclip
  Paperclip.interpolates :user_id  do |attachment, style|
    attachment.instance.user_id
  end

end
