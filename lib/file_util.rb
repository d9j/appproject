
class FileUtil
  def self.generate_hash
    name = SecureRandom.hex(rand(2..3)).to_i(16).to_s(36)
    second = SecureRandom.hex(rand(2..3)).to_i(16).to_s(36).upcase
     (name+second).split("").shuffle.join
  end
end