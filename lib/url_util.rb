class UrlUtil
  def self.is_url(string)
    uri = URI.parse(string)
    %w( http https ftp).include?(uri.scheme)
  rescue URI::BadURIError
    false
  rescue URI::InvalidURIError
    false
  end
end