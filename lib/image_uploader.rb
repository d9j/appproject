class ImageUploader
  def initialize(user)
    @user  = user
  end
  def upload(params)
    if params.has_key?(:img_url)
     return upload_by_url(params[:img_url])
    end
    upload_image(params[:file])
  end
  def upload_image(image)
    img = @user.images.build(image: image)
    img.save

    return img
  end

  def upload_by_url(img_url)
    img = @user.images.build
    img.picture_from_url(img_url)
    img.save

    return img
  end


end