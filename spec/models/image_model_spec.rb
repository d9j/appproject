require 'rails_helper'

RSpec.describe Image, type: :model do

  describe "Basic upload actions" do
    before(:all) do
      query_log
    end
    it "Should upload image file" do
      @user = FactoryGirl.create(:user,:user_with_images)
      hash = []
      @user.images.all.select(:file_hash).each do |img|
        hash.push(img.file_hash)
      end
      list = Image.list_images(hash)
      puts "List images #{list.inspect}" .colorize(:red)
    end
  end

end
