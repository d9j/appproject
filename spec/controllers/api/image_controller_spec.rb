require 'rails_helper'
include ActionDispatch::TestProcess
RSpec.describe Api::ImageController, :type => :controller do
  describe "Basic upload actions" do
    before(:all) do
      query_log
    end
    it "Should list images" do
      @user = FactoryGirl.create(:user, :user_with_images)
      hash = []
      @user.images.all.select(:file_hash).each do |img|
        hash.push(img.file_hash)
      end
      img_num = hash.length
      post :list, {:images => hash.join(",")}
      resp =  parse_json(response)
      expect(resp["img_list"].length).to eq(img_num)
    end
    it "Should find no images" do
      @user = FactoryGirl.create(:user)
      post :list, {:images =>""}
      expect(response.status) .to eq(400)
    end
  end
end