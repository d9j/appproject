require 'rails_helper'
include ActionDispatch::TestProcess
RSpec.describe Api::UploaderController, :type => :controller do
  describe "Basic upload actions" do
    before(:all) do
      query_log
    end
    it "Should upload image file" do
      @image = fixture_file_upload('dummy_img.jpg', 'image/jpeg')
      expect{
        post :img_upload, :file => @image
      }.to change(Image,:count).by(1)
      resp =  parse_json(response)
      expect(resp['response']).to eq(I18n.t('upload_results.upload_successful'))
    end
    it "Should upload image by url" do
      expect{
        post :image_url, :img_url => "http://i.imgur.com/PUv5PEh.png"
      }.to change(Image,:count).by(1)
      resp =  parse_json(response)
      expect(resp['response']).to eq(I18n.t('upload_results.upload_successful'))
    end
    it "Should fail to upload image file" do
      @image = fixture_file_upload('centos.sh')
      expect{
        post :img_upload, :file => @image
      }.to change(Image,:count).by(0)
      resp =  parse_json(response)
      validation_error = resp["response"]
      expect(validation_error).to eq(I18n.t('upload_results.failed_upload'))
    end
    it "Should fail to upload image by url" do
      expect{
        post :image_url, :img_url => "https://www.linkedin.com"
      }.to change(Image,:count).by(0)
      resp =  parse_json(response)
      validation_error = resp["response"]
      expect(validation_error).to eq(I18n.t('upload_results.failed_upload'))
    end
  end
end