require 'faker'
include ActionDispatch::TestProcess
FactoryGirl.define do
  factory :user_images, :class => Image do
    image { fixture_file_upload(Rails.root.join('spec/fixtures/dummy_img.jpg'), 'image/jpeg')}
  end
end