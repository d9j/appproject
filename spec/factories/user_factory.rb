require 'faker'
include ActionDispatch::TestProcess
FactoryGirl.define do
  factory :user,:class => User  do
    session_id SecureRandom.uuid
  end
  trait :user_with_images do
    after(:create) do |u|
      (0..3).each do
        FactoryGirl.create(:user_images, user: u)
      end
    end
  end
end